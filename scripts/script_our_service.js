// * our-services
const tabNav = document.querySelectorAll('.our-services-item');
const tabContent = document.querySelectorAll('.our-services-specialists');

tabNav.forEach(function (item){
    item.addEventListener("click", function (link) {
        link.preventDefault();

    let currentBtn = item;
    let tabId = currentBtn.getAttribute("data-tab");
    let currentTab = document.querySelector(tabId);

    if (! currentBtn.classList.contains('is-active')) {

        tabNav.forEach(function (item) {
            item.classList.remove('is-active');
        });

        tabContent.forEach(function (item) {
            item.classList.remove('is-active');
        });

        currentBtn.classList.add('is-active');
        currentTab.classList.add('is-active');
    }

    });
});








