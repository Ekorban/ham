
$(document).ready(function(){
    $(".owl-carousel").owlCarousel({
        items: 4,
        loop: true,
        nav: true,
        dots: false,
        center: true,
        navText: ['<svg class="owl-prev" width="31" height="31" viewBox="0 0 31 31" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
        '<g id="Preview copy" clip-path="url(#clip0_2143_60)">\n' +
        '<rect id="Rectangle 67" width="31" height="31" fill="black" fill-opacity="0.01" stroke="#BBBBBB"/>\n' +
        '<path id="Forma 1" fill-rule="evenodd" clip-rule="evenodd" d="M17.5076 21.5076L18.8126 20.1056L14.5246 15.4963L18.8126 10.8899L17.5076 9.4867L11.9146 15.4963L17.5076 21.5076Z" fill="#BBBBBB"/>\n' +
        '</g>\n' +
        '<defs>\n' +
        '<clipPath id="clip0_2143_60">\n' +
        '<rect width="31" height="31" fill="white"/>\n' +
        '</clipPath>\n' +
        '</defs>\n' +
        '</svg>', '<svg class="owl-next" width="31" height="31" viewBox="0 0 31 31" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
        '<g id="Preview copy" clip-path="url(#clip0_2143_60)">\n' +
        '<rect id="Rectangle 67" width="31" height="31" fill="black" fill-opacity="0.01" stroke="#BBBBBB"/>\n' +
        '<path id="Forma 1" fill-rule="evenodd" clip-rule="evenodd" d="M17.5076 21.5076L18.8126 20.1056L14.5246 15.4963L18.8126 10.8899L17.5076 9.4867L11.9146 15.4963L17.5076 21.5076Z" fill="#BBBBBB"/>\n' +
        '</g>\n' +
        '<defs>\n' +
        '<clipPath id="clip0_2143_60">\n' +
        '<rect width="31" height="31" fill="white"/>\n' +
        '</clipPath>\n' +
        '</defs>\n' +
        '</svg>'],
        slideBy: 1,
    });
});
