//* works filter
const filterBox = document.querySelector('.our-work-list');
const items = document.querySelectorAll('.box');

function filter() {
filterBox.addEventListener('click', event => {
    const filterClass = event.target.dataset.category

       switch (filterClass) {
        case 'all':
            getItems ('box');
            break
        case 'gd':
            getItems (filterClass);
            break
        case 'wd' :
            getItems (filterClass);
            break
        case 'lp' :
            getItems (filterClass);
            break
        case 'wordpress' :
            getItems (filterClass);
            break
    }
})

}
filter()

//* add images
function getItems (className) {
    items.forEach(item => {
        if (item.classList.contains(className)){
            item.style.display = 'block'
        } else {
            item.style.display = 'none'
        }
    })
}


//* our-amazing-work
let btn = document.querySelector('.load_btn');
let imgVisible = document.querySelectorAll('.hidden-works');

btn.addEventListener("click", event => {
    imgVisible.forEach(function (item) {
        item.classList.replace('hidden-works', 'visible-works')
    });
    btn.remove()
});

