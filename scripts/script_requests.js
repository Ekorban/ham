const tabSlider = document.querySelectorAll('.clients-frame');
const tabRequest = document.querySelectorAll('.clients-requests');

tabSlider.forEach(function (item) {
        item.addEventListener("click", function () {

            let currentArrow = item;
            let sliderId = currentArrow.getAttribute("data-slide");
            let sliderTab = document.querySelector(sliderId);

            if (!currentArrow.classList.contains('shown') && !currentArrow.classList.contains('picture')) {

                tabSlider.forEach(function (item) {
                    item.classList.remove('shown');
                    item.classList.remove('picture')
                });
                tabRequest.forEach(function (item) {
                    item.classList.remove('shown');
                    item.classList.remove('picture');
                });

                currentArrow.classList.add('shown');
                currentArrow.classList.add('picture')
                sliderTab.classList.add('shown');
            }
        });
    })